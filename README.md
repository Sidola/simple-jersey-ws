## Simple web service using Jersey

### Description

This is a very simple web service written in Java using the Jersey framework.

For details about how the Jersey framework works, refer to the documentation found here: https://jersey.java.net/documentation/latest/user-guide.html

### Requirements

- Java SDK 8u45 or newer
- Eclipse

### Setup

- Configure your host and port in the `Server.java` file if needed
- Run `Server.java` to start the server

### Usage

Once the server is up and running, you can test it by making calls to the different services. Below is a list of the available services and how to call them.

#### Adding digits
- Usage: `http://host:port/api/math/add?digit=10&digit=3`
- Result: `{"sum":13}`

#### Subtracting digits
- Usage: `http://host:port/api/math/subtract?digit=10&digit=3`
- Result: `{"sum":7}`

#### Convert a string to upper-case
- Usage: `http://host:port/api/string/uppercase?string=Hello`
- Result: `{"string":"HELLO"}`

#### Convert a string to lower-case
- Usage: `http://host:port/api/string/lowercase?string=Hello`
- Result: `{"string":"hello"}`

#### Reverse a string
- Usage: `http://host:port/api/string/reverse?string=Hello`
- Result: `{"string":"elloH"}`

### General program structure

The program is composed of two main parts. The server, and the services.

---

**`Server.java`** - This class is the http server that will host the web service. It takes resource-classes and starts the server.

---

**`MathService.java`** - This is one of the resource-classes `Server.java` is able to load and use for the web service. It will process calls made to the `host:port/api/math` URL.

---

**`StringService.java`** - Another resource-class used by the server. This one is located at `host:port/api/string`.