package webservice;

import java.net.URI;
import java.util.logging.Logger;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import webservice.services.MathService;
import webservice.services.StringService;

import com.sun.net.httpserver.HttpServer;

/**
 * This is the server that runs the web-service.
 * 
 * @author Sid
 */
public class Server {

    private final String HOST = "localhost";
    private final int PORT = 4243;

    private final Logger LOG = Logger.getLogger(Server.class.getName());

    /**
     * Constructor.
     * 
     * This method configures and creates the HTTP server that will be used for
     * the web-service.
     */
    public Server() {
        // Generate a base URI for the server
        URI baseURI = UriBuilder.fromUri(String.format("http://%s/api", HOST)).port(PORT)
                .build();

        // Add the services we will provide
        ResourceConfig config = new ResourceConfig(MathService.class, StringService.class);

        // Create and fire up the server
        HttpServer server = JdkHttpServerFactory.createHttpServer(baseURI, config);

        LOG.info("Server started...");
    }

    /**
     * Main method.
     * 
     * @param args
     */
    public static void main(String[] args) {
        Server server = new Server();
    }
}
