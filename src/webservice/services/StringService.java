package webservice.services;

import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

/**
 * This class is responsible for all the string-related services provided by the
 * web-service.
 * <p>
 * URI: <code>http://host:port/api/string</code>
 * 
 * @author Sid
 */
@Path("/string")
public class StringService {

    private final Logger LOG = Logger.getLogger(this.getClass().getName());

    /**
     * Reverses a string.
     * <p>
     * URI: <code>http://host:port/api/string/reverse</code>
     * <p>
     * Usage: <code>http://host:port/api/string/reverse?string=Hello</code> <br>
     * Returns: <code>{"string": "elloH"}</code>
     * 
     * @param string
     *            The string to reverse.
     * @return A JSON message with the reversed string.
     */
    @GET
    @Path("/reverse")
    @Produces(MediaType.APPLICATION_JSON)
    public Response reverseString(@QueryParam("string") String string) {
        LOG.info("Reversing string...");

        JSONObject json = new JSONObject();
        json.put("string", new StringBuilder(string).reverse().toString());
        return Response.ok().header("Access-Control-Allow-Origin", "http://localhost")
                .entity(json.toString()).build();
    }

    /**
     * Converts a string to upper-case.
     * <p>
     * URI: <code>http://host:port/api/string/uppercase</code>
     * <p>
     * Usage: <code>http://host:port/api/string/uppercase?string=Hello</code> <br>
     * Returns: <code>{"string": "HELLO"}</code>
     * 
     * @param string
     *            The string to convert.
     * @return A JSON message containing the converted string.
     */
    @GET
    @Path("/uppercase")
    @Produces(MediaType.APPLICATION_JSON)
    public Response uppercaseString(@QueryParam("string") String string) {
        LOG.info("Converting string to upper-case...");

        JSONObject json = new JSONObject();
        json.put("string", string.toUpperCase());
        return Response.ok().header("Access-Control-Allow-Origin", "http://localhost")
                .entity(json.toString()).build();
    }

    /**
     * Converts a string to lower-case.
     * <p>
     * URI: <code>http://host:port/api/string/lowercase</code>
     * <p>
     * Usage: <code>http://host:port/api/string/lowercase?string=Hello</code> <br>
     * Returns: <code>{"string": "hello"}</code>
     *
     * @param string
     *            The string to convert.
     * @return A JSON message containing the converted string.
     */
    @GET
    @Path("/lowercase")
    @Produces(MediaType.APPLICATION_JSON)
    public Response lowercaseString(@QueryParam("string") String string) {
        LOG.info("Converting string to lower-case...");

        JSONObject json = new JSONObject();
        json.put("string", string.toLowerCase());
        return Response.ok().header("Access-Control-Allow-Origin", "http://localhost")
                .entity(json.toString()).build();
    }
}
