package webservice.services;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.json.JSONObject;

/**
 * This class is responsible for all the math-related services provided by the
 * web-service.
 * <p>
 * URI: <code>http://host:port/api/math</code>
 * 
 * @author Sid
 */
@Path("/math")
public class MathService {

    private final Logger LOG = Logger.getLogger(this.getClass().getName());

    /**
     * Adds all the given numbers and returns the sum.
     * <p>
     * URI: <code>http://host:port/api/math/add</code>
     * <p>
     * Usage: <code>http://host:port/api/math/add?digit=10&digit=10</code> <br>
     * Returns: <code>{"sum": 20}</code>
     * 
     * @param info
     *            List of URL parameters to be added, is injected automatically.
     * @return Returns a JSON message containing the added sum of the 'digit'
     *         parameters.
     */
    @GET
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(@Context UriInfo info) {
        LOG.info("Adding digits...");

        JSONObject json = new JSONObject();
        List<String> params = info.getQueryParameters().get("digit");

        long retVal = 0;
        for (String string : params) {
            try {
                retVal += Long.valueOf(string);
            } catch (NumberFormatException e) {
                json.put("error", "One or more params is not a valid number.");

                return Response.serverError()
                        .header("Access-Control-Allow-Origin", "http://localhost")
                        .entity(json.toString()).build();
            }
        }

        json.put("sum", retVal);
        return Response.ok().header("Access-Control-Allow-Origin", "http://localhost")
                .entity(json.toString()).build();
    }

    /**
     * Performs a subtraction on all the given numbers and returns the sum.
     * <p>
     * URI: <code>http://host:port/api/math/subtract</code>
     * <p>
     * Usage: <code>http://host:port/api/math/subtract?digit=20&digit=10</code>
     * <br>
     * Returns: <code>{"sum": 10}</code>
     * 
     * @param info
     *            List of URL parameters to subtract, is injected automatically.
     * @return Returns a JSON message containing the sum of the subtraction.
     */
    @GET
    @Path("/subtract")
    @Produces(MediaType.APPLICATION_JSON)
    public Response subtract(@Context UriInfo info) {
        LOG.info("Subtracting digits...");

        JSONObject json = new JSONObject();
        List<String> params = info.getQueryParameters().get("digit");

        // Grab the first value and remove it
        long retVal = Long.valueOf(params.get(0));
        params.remove(0);

        for (String string : params) {
            try {
                retVal -= Long.valueOf(string);
            } catch (NumberFormatException e) {
                json.put("error", "One or more params is not a valid number.");

                return Response.serverError()
                        .header("Access-Control-Allow-Origin", "http://localhost")
                        .entity(json.toString()).build();
            }
        }

        json.put("sum", retVal);
        return Response.ok().header("Access-Control-Allow-Origin", "http://localhost")
                .entity(json.toString()).build();
    }
}
